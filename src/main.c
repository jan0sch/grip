/* main.c
 *
 * Copyright (c) 1998-2004  Mike Oliphant <grip@nostatic.org>
 *
 *   http://www.nostatic.org/grip
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
 * USA
 */

#include <config.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <stdlib.h>

#include "grip.h"

static gint TimeOut(gpointer data);

gboolean do_debug=TRUE;
GtkWidget* grip_app;

/* popt table */
static char *config_filename=NULL;
static char *device=NULL;
static char *scsi_device=NULL;
static int force_small=FALSE;
static int local_mode=FALSE;
static int no_redirect=FALSE;
static int verbose=FALSE;

G_GNUC_NORETURN static gboolean
option_version_cb(const gchar  *option_name,
                  const gchar  *value,
                  gpointer      data,
                  GError      **error)
{
  g_print("%s %s\n", PACKAGE, VERSION);
  exit(0);
}

static const GOptionEntry options[] = {
  {
    "config",
    'c', 0,
    G_OPTION_ARG_STRING,
    &config_filename,
    N_("Specify the config file to use (in your home dir)"),
    N_("CONFIG")
  },
  {
    "device",
    'd', 0,
    G_OPTION_ARG_STRING,
    &device,
    N_("Specify the cdrom device to use"),
    N_("DEVICE")
  },
  {
    "scsi-device",
    0, 0,
    G_OPTION_ARG_STRING,
    &scsi_device,
    N_("Specify the generic scsi device to use"),
    N_("DEVICE")
  },
  {
    "small",
    's', 0,
    G_OPTION_ARG_NONE,
    &force_small,
    N_("Launch in \"small\" (cd-only) mode"),
    NULL
  },
  {
    "local",
    'l', 0,
    G_OPTION_ARG_NONE,
    &local_mode,
    N_("\"Local\" mode -- do not look up disc info on the net"),
    NULL
  },
  {
    "no-redirect",
    0, 0,
    G_OPTION_ARG_NONE,
    &no_redirect,
    N_("Do not do I/O redirection"),
    NULL
  },
  {
    "verbose",
    'v', 0,
    G_OPTION_ARG_NONE,
    &verbose,
    N_("Run in verbose (debug) mode"),
    NULL
  },
  {
    "version",
    0, G_OPTION_FLAG_NO_ARG | G_OPTION_FLAG_HIDDEN,
    G_OPTION_ARG_CALLBACK,
    option_version_cb,
    NULL,
    NULL
  },
  { NULL }
};

void Debug(char *fmt,...)
{
  va_list args;
  char *msg;

  if(do_debug) {
    va_start(args,fmt);

    msg=g_strdup_vprintf(fmt,args);
    if(msg) {
      g_printerr("%s", msg);
      g_free(msg);
    }
  }

  va_end(args);
}

int Cmain(int argc, char* argv[])
{
  GOptionContext *context;

  /* Unbuffer stdout */
  setvbuf(stdout, 0, _IONBF, 0);

  /* setup locale, i18n */
  gtk_set_locale();
  bindtextdomain(GETTEXT_PACKAGE,GNOMELOCALEDIR);
  textdomain(GETTEXT_PACKAGE);

  context = g_option_context_new (NULL);
  g_option_context_add_main_entries (context, options, GETTEXT_PACKAGE);
  g_option_context_set_ignore_unknown_options (context, TRUE);
  g_option_context_parse (context, &argc, &argv, NULL);

  gtk_init(&argc, &argv);

  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF8");
  setenv("CHARSET","UTF-8",1);

  do_debug=verbose;

  if(scsi_device) printf("scsi=[%s]\n",scsi_device);

  /* Start a new Grip app */
  grip_app=GripNew(device,scsi_device,config_filename,
		   force_small,local_mode,
		   no_redirect);

  gtk_widget_show(grip_app);

  gtk_timeout_add(1000,TimeOut,0);

  gtk_main();

  return 0;
}

static gint TimeOut(gpointer data)
{
  GripUpdate(grip_app);

  return TRUE;
}
