project('grip', ['c', 'cpp'],
  version: '4.2.4',
  license: 'GPL2+',
  meson_version: '>= 0.47.0'
)

grip_prefix = get_option('prefix')
grip_datadir = join_paths(grip_prefix, get_option('datadir'))
grip_icondir = join_paths(grip_datadir, 'pixmaps')
grip_localedir = join_paths(grip_datadir, 'locale')
grip_helpdir = join_paths(grip_datadir, 'gnome', 'help', 'grip')

config_h = configuration_data()

config_h.set_quoted('PACKAGE', 'grip')
config_h.set_quoted('VERSION', meson.project_version())
config_h.set('GETTEXT_PACKAGE', 'PACKAGE')
config_h.set_quoted('PACKAGE_BUGREPORT', 'https://sourceforge.net/projects/grip')
config_h.set_quoted('GNOME_ICONDIR', grip_icondir)
config_h.set_quoted('GNOMELOCALEDIR', grip_localedir)

cc = meson.get_compiler('c')
cxx = meson.get_compiler('cpp')

libgtk_dep = dependency('gtk+-2.0', version: '>= 2.14')
id3_dep = cxx.find_library('libid3', required: get_option('id3lib'))
curl_dep = dependency('libcurl')
thread_dep = dependency('threads')

cdparanoia_deps = []
cdparanoia_found = false
cdparanoia_dep = dependency('cdparanoia-3', version: '>= 10.2', required: get_option('cdparanoia'))

if cdparanoia_dep.found()
  cdparanoia_deps = [cdparanoia_dep]
  cdparanoia_found = true
else
  cdparanoia_dep = cc.find_library('cdda_paranoia', required : get_option('cdparanoia'))
  cdinterface_dep = cc.find_library('cdda_interface', required : get_option('cdparanoia'))
  if cdparanoia_dep.found() and cdinterface_dep.found()
    cdparanoia_deps = [cdparanoia_dep, cdinterface_dep]
    cdparanoia_found = true
  endif
endif

if cdparanoia_found
  # TODO: convert this to a CDPARANOIA_HEADERS_IN_DIR approach
  config_h.set('HAVE_CDDA_INTERFACE_H', cc.has_header('cdda_paranoia.h'))
  config_h.set('HAVE_CDDA_PARANOIA_H', cc.has_header('cdda_paranoia.h'))
  config_h.set('HAVE_CDDA_CDDA_INTERFACE_H', cc.has_header('cdda/cdda_interface.h'))
  config_h.set('HAVE_CDDA_CDDA_PARANOIA_H', cc.has_header('cdda/cdda_paranoia.h'))
endif

check_headers = [
  ['HAVE_SYS_VFS_H', 'sys/vfs.h'],
  ['HAVE_SYS_STATVFS_H', 'sys/statvfs.h'],
  ['HAVE_LINUX_CDROM_H', 'linux/cdrom.h'],
  ['HAVE_SYS_CDIO_H', 'sys/cdio.h'],
  ['HAVE_IO_CAM_CDROM_H', 'io/cam/cdrom.h'],
  ['HAVE_SYS_MNTENT_H', 'sys/mntent.h'],
]
foreach header: check_headers
  config_h.set(header[0], cc.has_header(header[1]))
endforeach

configure_file(output: 'config.h', configuration: config_h)
config_inc = include_directories('.')

add_global_arguments(
  '-DG_LOG_DOMAIN="@0@"'.format(meson.project_name()),
  language : ['c', 'cpp']
)

gnome = import('gnome')
i18n = import('i18n')

subdir('src')
subdir('pixmaps')
subdir('po')
subdir('doc/C')

applicationsdir = join_paths(grip_datadir, 'applications')
install_data('grip.desktop', install_dir: applicationsdir)
install_data('grip-audiocd.desktop', install_dir: join_paths(grip_datadir, 'apps', 'solid', 'actions'))
install_data('grip-audiocd.desktop', install_dir: join_paths(grip_datadir, 'solid', 'actions'))

install_man('grip.1')
