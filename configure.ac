AC_PREREQ([2.60])

AC_INIT([grip], [4.2.4], [https://sourceforge.net/projects/grip])
AC_CONFIG_SRCDIR([src/main.c])
AC_CONFIG_MACRO_DIRS([m4])
AC_CONFIG_AUX_DIR([build-aux])
AM_INIT_AUTOMAKE([1.14 foreign])

AM_MAINTAINER_MODE
AM_CONFIG_HEADER(config.h)

AC_ARG_ENABLE(shared_cdpar,
	[AS_HELP_STRING([--disable-shared-cdpar], [use static cdparanoia lib])],
        ,enable_shared_cdpar=yes)

AC_ARG_ENABLE(cdpar,
	[AS_HELP_STRING([--disable-cdpar],[do not compile with cdparanoia])],
        , enable_cdpar=yes)

AC_ARG_ENABLE(id3,
	[AS_HELP_STRING([--disable-id3], [do not compile with id3lib])],
        , enable_id3=yes)

AC_ARG_ENABLE(shared_id3,
	[AS_HELP_STRING([--disable-shared-id3],[use static id3lib])],
        , enable_shared_id3=yes)

warning_flags="$CFLAGS -Wall"
dnl glib deprecated a function that gtk2 still uses
dnl disable deprecation warnings for now
warning_flags="$warning_flags -Wno-deprecated-declarations"
werror_flags="$warning_flags -Werror"

AC_ARG_ENABLE([werror],
	[AS_HELP_STRING([--enable-werror],[treat warnings as errors])],
        , enable_werror=no)

AS_IF([test $enable_werror = yes],
      [CFLAGS="$CFLAGS $werror_flags"],
      [CFLAGS="$CFLAGS $warning_flags"])

AC_PROG_CC
AC_PROG_CXX
AC_ISC_POSIX
AC_HEADER_STDC
AC_CHECK_HEADERS(sys/vfs.h sys/statvfs.h)
AC_CHECK_HEADERS(linux/cdrom.h sys/cdio.h io/cam/cdrom.h sys/mntent.h)

AM_PROG_LIBTOOL

PKG_PROG_PKG_CONFIG
PKG_CHECK_MODULES(GNOME, gtk+-2.0 >= 2.14,
                  [LIBS="$LIBS $GNOME_LIBS"
                   CFLAGS="$CFLAGS $GNOME_CFLAGS"])

dnl check for ghttp
AC_CHECK_HEADER(curl/curl.h, [], [AC_MSG_ERROR(libcurl headers are missing)])
AC_CHECK_LIB(curl,curl_global_init, [LIBS="$LIBS `curl-config --libs`"], [AC_MSG_ERROR(curl libs are missing)])

GETTEXT_PACKAGE=grip
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Gettext package])
AM_GNU_GETTEXT([external])
AM_GNU_GETTEXT_VERSION([0.19.4])

dnl pthread check
AC_CHECK_HEADER(pthread.h,
  AC_CHECK_LIB(pthread, pthread_join,
    [THREAD_LIBS="-lpthread"],
    [AC_MSG_ERROR(pthread library is missing)]),
  [AC_MSG_ERROR(pthread.h header is missing)])

AC_SUBST(THREAD_LIBS)

dnl do cdparanoia check
if test "$enable_cdpar" = "yes"; then
  LIBS="$LIBS -lm"

  AC_CHECK_HEADERS([cdda_interface.h cdda_paranoia.h], [], [],
    [[#ifdef HAVE_CDDA_INTERFACE_H
    # include <cdda_interface.h>
    #endif
    #ifdef HAVE_CDDA_PARANOIA_H
    # include <cdda_paranoia.h>
    #endif
    ]])
  AC_CHECK_HEADERS([cdda/cdda_interface.h cdda/cdda_paranoia.h], [], [],
    [[#ifdef HAVE_CDDA_CDDA_INTERFACE_H
    # include <cdda/cdda_interface.h>
    #endif
    #ifdef HAVE_CDDA_CDDA_PARANOIA_H
    # include <cdda/cdda_paranoia.h>
    #endif
    ]])

  AC_CHECK_LIB(cdda_interface,main,
    if test "$enable_shared_cdpar" = "yes"; then
      echo "using shared cdparanoia libraries"
      CDPAR_LIBS="-lcdda_interface -lcdda_paranoia"
    else
      echo "using static cdparanoia libraries"
      CDPAR_LIBS="$prefix/lib/libcdda_interface.a $prefix/lib/libcdda_paranoia.a"
    fi)
fi
AC_SUBST(CDPAR_LIBS)

id3_libs_bak=$LIBS
if test "$enable_id3" = "yes"; then
	AC_SEARCH_LIBS(ID3Tag_Link,"id3 -lz -lstdc++",
		id3lib=yes
		AC_DEFINE(HAVE_ID3V2, [], [Description])
		if test "$enable_shared_id3" = "yes"; then
		  echo "using shared id3lib"
		  ID3_LIBS="-lid3 -lz -lstdc++"
	        else
		  echo "using static id3lib"
		  ID3_LIBS="$prefix/lib/libid3.a -lz -lstdc++"
	    fi,
		id3lib=no
		AC_MSG_WARN(id3lib not found - disabling id3v2 support))
fi

AC_SUBST(ID3_LIBS)
LIBS=$id3_libs_bak

AC_SUBST(CFLAGS)
AC_SUBST(CPPFLAGS)
AC_SUBST(LDFLAGS)

AC_OUTPUT([ po/Makefile.in
Makefile
src/Makefile
pixmaps/Makefile
doc/Makefile
doc/C/Makefile
contrib/Makefile
])
